<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $items = Item::all();
        Log::info('Item index', array($items));
        return [$items];
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $item = new Item();
        $input = $request->all();
        $item->fill($input)->save();
        return ['created' => true, 'id' => $item->id];
    }


    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        $field = $request->all();
        $item->fill($field)->save();

        return ['updated' => true];
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function destroy($id)
    {
        try {
            $item = Item::find($id);
            $item->delete();
            return ['deleted' => true];
        } catch (QueryException $e) {
            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'This Item can\'t be deleted';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }

    }
}
