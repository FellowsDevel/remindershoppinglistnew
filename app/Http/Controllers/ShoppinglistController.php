<?php

namespace App\Http\Controllers;

use App\Item;
use App\ShoppingList;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShoppinglistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return array
     */
    public function index()
    {
        $list = ShoppingList::with('items')->where('user_id', Auth::id())->get();
//        Log::info('Shopping list index', array($list));
        return [$list];

    }

    public function create()
    {
    }

    public function store(Request $request)
    {

        $list = new Shoppinglist();
        $list->name = $request['name'];
        $list->user_id = $request['user_id'];
        $list->save();

//        Log::info('Store request - name    : ', array($request->json('name')));
//        Log::info('Store request - user_id :', array($request->json('user_id')));
        return ['created' => true, 'id' => $list->id];
    }

    public function edit($id)
    {
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        $list =  Shoppinglist::findOrFail($id)->where("user_id", Auth::id())->get()[0];
        if(!$list){
            return ['error_code' => -1, 'error_msg' => 'user list not found'];
        }
        $fields = $request->all();
        $list->fill($fields)->save();
        return ['updated' => true];

    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function destroy($id)
    {
        try {
            $list = Shoppinglist::find($id)->where("user_id", Auth::id());
            $list->delete();
            return ['deleted' => true];
        } catch (QueryException $e) {
            $error_code = $e->errorInfo[1];
            $error_msg = '';
            if ($error_code == 1451) {
                $error_msg = 'This Shoppinglist can\'t be deleted';
            }
            return ['error_code' => $error_code, 'error_msg' => $error_msg];
        }
    }

    /**
     * @param Request $request
     * @param $list_id
     * @return array
     */
    public function addItem(Request $request, $list_id)
    {
        $list =  Shoppinglist::findOrFail($list_id)->where("user_id", Auth::id())->get()[0];

        foreach ($request->id as $id) {
            $item = Item::findOrFail($id);
//            Log::info('item ->', array($item));
            $list->items()->save($item);
            $list->save();
        }

//        $lista2 = Lista::with('items')->findOrFail($list_id);
//        Log::info('AddItem items', array($lista2->items()->get()));

        return ['added' => true];
    }

    /**
     * @param Request $request
     * @param $list_id
     * @return array
     */
    public function removeItem(Request $request, $list_id)
    {
        $list =  Shoppinglist::findOrFail($list_id)->where("user_id", Auth::id())->get()[0];

        foreach ($request->id as $id) {
            $item = Item::findOrFail($id);
//            Log::info('item ->', array($item));
            $list->items()->delete($item);
            $list->save();
        }

//        $lista2 = Lista::with('items')->findOrFail($list_id);
//        Log::info('AddItem items', array($lista2->items()->get()));

        return ['added' => true];
    }
}
