<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingList extends Model
{
    protected $fillable = ['user_id', 'name', 'warning_days'];

    public function items()
    {
        return $this->belongsToMany(
            Item::class,
            'item_shoppinglist',
            'shoppinglist_id',
            'item_id'
        )->withPivot('checked');
    }

    public function checkOut()
    {
        // TODO: move to pending list
        $chkds = $this->items()->wherePivot('checked', '=', '1')->get();
        foreach ($chkds as $chkd) {
            $this->items()->detach($chkd->id);
        }
    }

    private function checkItem($itemList, $value)
    {
        $lista = $this->items();
        foreach ($itemList as $item) {
            $lista->updateExistingPivot($item, ['checked' => $value]);
        }
    }

    public function checkItems($itemList)
    {
        $this->checkItem($itemList, '1');
    }

    public function uncheckItems($itemList)
    {
        $this->checkItem($itemList, '0');
    }
}
