<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateItemShoppinglistPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_shoppinglist', function (Blueprint $table) {
            $table->integer('item_id')->unsigned()->index();
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->integer('shoppinglist_id')->unsigned()->index();
            $table->foreign('shoppinglist_id')->references('id')->on('shoppinglists')->onDelete('cascade');
            $table->boolean('checked')->default(false);
            $table->primary(['item_id', 'shoppinglist_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item_shoppinglist');
    }
}
